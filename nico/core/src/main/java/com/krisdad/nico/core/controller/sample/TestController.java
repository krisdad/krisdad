package com.krisdad.nico.core.controller.sample;

import com.krisdad.nico.common.bean.FeignTest;
import com.krisdad.nico.common.bean.ResponseResult;
import com.krisdad.nico.core.feign.UserTestFeignService;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;

/**
 * @author HLX
 * @version 1.0
 * @date 2021/5/8 11:03
 */
@RestController
@RequestMapping("/test")
public class TestController {

  @Resource private RestTemplate restTemplate;

  @Resource private DiscoveryClient discoveryClient;

  @Resource private UserTestFeignService userTestFeignService;

  @GetMapping("/{value}")
  public ResponseResult test(@PathVariable("value") String value) {
    List<ServiceInstance> instances = discoveryClient.getInstances("user");
    ServiceInstance serviceInstance = instances.get(0);
    URI uri = serviceInstance.getUri();
    System.out.println("uri = " + uri.toString());
    Object forObject =
        restTemplate.getForObject(serviceInstance.getUri() + "/test/" + value, Object.class);
    return ResponseResult.success(forObject);
  }

  @GetMapping("/loadBalance/{value}")
  public ResponseResult loadBalance(@PathVariable("value") String value) {
    String userServiceUrl = "http://user";
    Object forObject = restTemplate.getForObject(userServiceUrl + "/test/" + value, Object.class);
    return ResponseResult.success(forObject);
  }

  @GetMapping("/feign-test/value")
  public ResponseResult feign() {
    return ResponseResult.success(userTestFeignService.feignTest1());
  }

  @RequestMapping("/feign/{value}")
  public ResponseResult feign(@PathVariable("value") String value) {
    return ResponseResult.success(userTestFeignService.feignTest2(value));
  }

  @RequestMapping("/feign-obj")
  public ResponseResult feign(FeignTest feignTest) {
    return ResponseResult.success(userTestFeignService.feignTest3(feignTest));
  }
}
