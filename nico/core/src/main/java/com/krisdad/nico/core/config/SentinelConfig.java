package com.krisdad.nico.core.config;

import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/12 15:22
 */
//@Configuration
public class SentinelConfig implements ApplicationRunner {

  @Override
  public void run(ApplicationArguments args) throws Exception {
    initFlowRules();
  }

  private void initFlowRules() {
    List<FlowRule> rules = new ArrayList<>();

    FlowRule flowRule = new FlowRule();
    // 设置限流资源名称
    flowRule.setResource("sentinel_qps_and_strategy_direct");
    // 设置限流的阈值类型 FLOW_GRADE_QPS|FLOW_GRADE_THREAD 默认为QPS
    flowRule.setGrade(RuleConstant.FLOW_GRADE_QPS);
    // 设置单机阈值
    flowRule.setCount(1);
    // 设置限流模式 默认为直接
//    flowRule.setStrategy(RuleConstant.STRATEGY_DIRECT);
    // 设置限流效果 0. default(reject directly), 1. warm up, 2. rate limiter, 3. warm up + rate limiter
    // 默认为快速失败
//    flowRule.setControlBehavior(RuleConstant.CONTROL_BEHAVIOR_DEFAULT);
    // 设置关联资源，用于限流模式为关联和链路使用
    // flowRule.setRefResource("/ref");
    rules.add(flowRule);
    FlowRuleManager.loadRules(rules);
  }
}
