package com.krisdad.nico.core.controller.sample;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.krisdad.nico.common.bean.ResponseResult;
import com.krisdad.nico.core.feign.UserTestFeignService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/12 15:40
 */
@RestController
@RequestMapping("/sentinel")
public class SentinelController {

  @Resource private UserTestFeignService userTestFeignService;

  //    @RequestMapping("/qps/strategy_direct")
  //    public ResponseResult qpsAndStrategyDirect1(){
  //        try(Entry entry = SphU.entry("/sentinel/qps/strategy_direct")){
  //            return ResponseResult.success();
  //        } catch (BlockException ex) {
  //            ex.printStackTrace();
  //            return ResponseResult.error("限流");
  //        }
  //    }

  @SentinelResource(value = "sentinel_qps_and_strategy_direct", blockHandler = "blockHandler")
  @RequestMapping("/qps/strategy_direct")
  public ResponseResult qpsAndStrategyDirect() {
    return ResponseResult.success();
  }

  public ResponseResult blockHandler(BlockException blockException) {
    blockException.printStackTrace();
    return ResponseResult.error("限流 blockHandler");
  }

  @RequestMapping("/feign/{value}")
  public ResponseResult feign(@PathVariable String value) {
    return ResponseResult.success(userTestFeignService.feign(value));
  }
}
