package com.krisdad.nico.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author HLX
 * @version 1.0
 * @date 2021/5/8 11:01
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class CoreApplication {
  public static void main(String[] args) {
    SpringApplication.run(CoreApplication.class,args);
  }
}
