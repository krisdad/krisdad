package com.krisdad.nico.core.feign;

import com.krisdad.nico.common.bean.FeignTest;
import com.krisdad.nico.common.bean.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/11 15:27
 */
@FeignClient(name = "user",fallback = SentinelFeignServiceFallbackImpl.class)
public interface UserTestFeignService {

  /**
   * 测试无参数feign调用
   *
   * @return 返回调用服务的端口号
   */
  @RequestMapping("/test/feign")
  Object feignTest1();

  /**
   * 测试路径参数feign调用
   *
   * @param value value
   * @return 返回value与服务的端口号
   */
  @RequestMapping("/test/feign/{value}")
  Object feignTest2(@PathVariable("value") String value);

  /**
   * 测试对象参数feign调用
   *
   * @param feignTest value
   * @return 返回value与服务的端口号
   */
  @PostMapping("/test/feign-obj")
  Object feignTest3(@RequestBody FeignTest feignTest);


  /**
   * User服务 Feign Sentinel 测试
   *
   * @param value 测试值
   * @return User 服务返回结果
   */
  @RequestMapping("/sentinel/feign/{value}")
  ResponseResult feign(@PathVariable String value);

}
