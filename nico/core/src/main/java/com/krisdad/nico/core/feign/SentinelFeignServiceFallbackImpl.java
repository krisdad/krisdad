package com.krisdad.nico.core.feign;

import com.krisdad.nico.common.bean.FeignTest;
import com.krisdad.nico.common.bean.ResponseResult;
import org.springframework.stereotype.Component;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/13 10:59
 */
@Component
public class SentinelFeignServiceFallbackImpl implements UserTestFeignService{
    /**
     * 测试无参数feign调用
     *
     * @return 返回调用服务的端口号
     */
    @Override
    public Object feignTest1() {
        return ResponseResult.error("user 服务忙");
    }

    /**
     * 测试路径参数feign调用
     *
     * @param value value
     * @return 返回value与服务的端口号
     */
    @Override
    public Object feignTest2(String value) {
        return ResponseResult.error("user 服务忙");
    }

    /**
     * 测试对象参数feign调用
     *
     * @param feignTest value
     * @return 返回value与服务的端口号
     */
    @Override
    public Object feignTest3(FeignTest feignTest) {
        return ResponseResult.error("user 服务忙");
    }

    /**
     * User服务 Feign Sentinel 测试
     *
     * @param value 测试值
     * @return User 服务返回结果
     */
    @Override
    public ResponseResult feign(String value) {
        return ResponseResult.error("user 服务忙");
    }
}
