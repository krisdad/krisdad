package com.krisdad.nico.common.bean;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/11 15:34
 */
@Data
public class FeignTest implements Serializable {

    private String value;
    private LocalDateTime requestTime;

}
