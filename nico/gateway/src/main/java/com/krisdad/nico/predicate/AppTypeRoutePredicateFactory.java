package com.krisdad.nico.predicate;

import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/14 14:18
 */
@Component
public class AppTypeRoutePredicateFactory extends AbstractRoutePredicateFactory<AppTypeRoutePredicateFactory.Config> {


    public AppTypeRoutePredicateFactory() {
        super(AppTypeRoutePredicateFactory.Config.class);
    }


    @Override
    public List<String> shortcutFieldOrder() {
        // 参数顺序,名称要与config里面定义的一致
        return Collections.singletonList("type");
    }

    @Override
    public Predicate<ServerWebExchange> apply(Config config) {
        return serverWebExchange -> {
            String app = serverWebExchange.getRequest().getQueryParams().get("app").get(0);
            if (StringUtils.isEmpty(app)){
                return false;
            }
            return app.equals(config.type);
        };
    }

    public static class Config{
        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
