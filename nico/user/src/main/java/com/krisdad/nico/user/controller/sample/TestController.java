package com.krisdad.nico.user.controller.sample;

import com.krisdad.nico.common.bean.FeignTest;
import com.krisdad.nico.common.bean.ResponseResult;
import com.krisdad.nico.user.config.ServiceInfoUtil;
import org.springframework.web.bind.annotation.*;

/**
 * @author HLX
 * @version 1.0
 * @date 2021/5/6 14:15
 */
@RestController
@RequestMapping("/test")
public class TestController {

  @GetMapping(value = "/{value}")
  public ResponseResult test(@PathVariable("value") String name) {
    return ResponseResult.success("test value = " + name+" port = "+ ServiceInfoUtil.getPort());
  }

  @GetMapping("/feign")
  public ResponseResult feign() {
    return ResponseResult.success("server port = "+ServiceInfoUtil.getPort());
  }

  @RequestMapping("/feign/{value}")
  public ResponseResult feign(@PathVariable("value") String value) {
    return ResponseResult.success("test value = " + value+" port = "+ ServiceInfoUtil.getPort());
  }

  @PostMapping("/feign-obj")
  public ResponseResult feign(@RequestBody FeignTest feignTest) {
    return ResponseResult.success(feignTest.toString()+" server port = "+ServiceInfoUtil.getPort());
  }
}
