package com.krisdad.nico.user.config;

import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

/**
 * @author krisdad
 */
@Configuration
public class ServiceInfoUtil implements ApplicationListener<WebServerInitializedEvent> {
    private static int serverPort;

    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        serverPort = event.getWebServer().getPort();
    }

    public static int getPort() {
        return serverPort;
    }
 }
