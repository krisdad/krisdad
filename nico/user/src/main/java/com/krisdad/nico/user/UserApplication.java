package com.krisdad.nico.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author HLX
 * @version 1.0
 * @date 2021/4/30 14:41
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class UserApplication {
  public static void main(String[] args) {
    SpringApplication.run(UserApplication.class,args);
  }
}
