package com.krisdad.nico.user.controller.sample;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.krisdad.nico.common.bean.ResponseResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/13 11:19
 */
@RestController
@RequestMapping("/sentinel")
public class SentinelController {

  @SentinelResource("feign-sentinel")
  @RequestMapping("/feign/{value}")
  public ResponseResult feign(@PathVariable String value) {
    return ResponseResult.success(value);
  }
}
