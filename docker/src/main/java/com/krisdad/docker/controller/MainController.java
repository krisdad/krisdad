package com.krisdad.docker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HLX
 * @version 1.0
 * @date 2021/4/29 16:23
 */
@RestController
@RequestMapping("/api/index")
public class MainController {

    @GetMapping("/test")
    public Object index(){
        return "hello world";
    }

}
