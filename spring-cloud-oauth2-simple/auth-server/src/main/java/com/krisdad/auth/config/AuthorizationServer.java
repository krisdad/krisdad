package com.krisdad.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.InMemoryAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

import javax.annotation.Resource;

/**
 * 授权服务配置
 *
 * @author krisdad
 * @version 1.0
 * @date 2021/5/27 11:35
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {

    /**
     * 密码编码器
     */
    @Resource
    private PasswordEncoder passwordEncoder;

    /**
     * Token 存储
     */
    @Resource
    private TokenStore tokenStore;

    /**
     * 认证管理器 密码模式才需要配置
     */
    @Resource
    private AuthenticationManager authenticationManager;

    /**
     * OAuth2 客户端服务
     */
    @Resource
    private ClientDetailsService clientDetailsService;


    /**
     * 注入TokenServices
     * @return tokenServices
     */
    @Bean
    public AuthorizationServerTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setClientDetailsService(clientDetailsService);
        defaultTokenServices.setSupportRefreshToken(true);

        //配置token的存储方法
        defaultTokenServices.setTokenStore(tokenStore);
        defaultTokenServices.setAccessTokenValiditySeconds(300);
        defaultTokenServices.setRefreshTokenValiditySeconds(1500);
        return defaultTokenServices;
    }

    /**
     * 注入授权码管理服务
     * @return 授权码管理服务
     */
    @Bean
    public AuthorizationCodeServices authorizationCodeServices(){
        return new InMemoryAuthorizationCodeServices();
    }

    /**
     * 配置客户端
     *
     * @param clients 客户端
     * @throws Exception exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                // client的id
                .withClient("client1")
                // 密码
                .secret(passwordEncoder.encode("123456"))
                // 给client一个id,这个在client的配置里要用的
                .resourceIds("resource1")
                // 配置认证类型 oauth2有4种类型
                .authorizedGrantTypes("authorization_code", "password", "client_credentials", "implicit", "refresh_token")
                //授权的范围,每个resource会设置自己的范围.
                .scopes("all")
                //这个是设置要不要弹出确认授权页面的.
                .autoApprove(false)
                //这个相当于是client的域名,重定向给code的时候会跳转这个域名
                .redirectUris("http://www.baidu.com");
    }

    //把上面的各个组件组合在一起

    /**
     * 配置认证端点
     * @param endpoints 端点
     * @throws Exception exception
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        // 配置认证管理器
        endpoints.authenticationManager(authenticationManager)
                // 配置授权码管理服务
                .authorizationCodeServices(authorizationCodeServices())
                // token服务
                .tokenServices(tokenServices())
                // 配置认证允许的http方法
                .allowedTokenEndpointRequestMethods(HttpMethod.POST,HttpMethod.GET);
    }

    //配置哪些接口可以被访问

    /**
     * 配置接口访问规则
     * @param security AuthorizationServerSecurityConfigurer
     * @throws Exception exception
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        // 公开 /oauth/token_key 公开
        security.tokenKeyAccess("permitAll()")
                // 公开 /oauth/check_token
                .checkTokenAccess("permitAll()")
                // 允许表单认证
                .allowFormAuthenticationForClients();
    }
}
