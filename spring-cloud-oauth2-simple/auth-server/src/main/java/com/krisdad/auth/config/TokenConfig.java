package com.krisdad.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

/**
 * Token 配置项
 *
 * @author krisdad
 * @version 1.0
 * @date 2021/5/27 14:57
 */
@Configuration
public class TokenConfig {

    /**
     * Token 存储方式
     * InMemoryTokenStore 存储在内存里面
     * RedisTokenStore 存储在Redis里面
     * JdbcTokenStore 存储在数据库里面
     *
     * @return
     */
    @Bean
    public TokenStore tokenStore() {
        return new InMemoryTokenStore();
    }

}
