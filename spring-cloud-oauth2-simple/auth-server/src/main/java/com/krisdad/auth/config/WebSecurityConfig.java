package com.krisdad.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Spring Security 配置
 * @author krisdad
 * @version 1.0
 * @date 2021/5/27 11:12
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 注入密码解析器
     * @return 密码解析器
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 注入认证管理器
     * 此处使用默认的认证管理器
     * @return  认证管理器
     * @throws Exception exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * 配置Security 规则
     * @param http HttpSecurity
     * @throws Exception ex
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 关闭csrf检测
        http.csrf().disable()
                // 开放所有接口，交给OAuth2来校验
                .authorizeRequests()
                .anyRequest().permitAll()
                // 打开表单登录
                .and()
                .formLogin();
    }
}
