package com.krisdad.auth.service;

import com.krisdad.auth.bean.User;
import com.krisdad.auth.mapper.UserMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Set;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/19 15:18
 */
@Service
public class SpringUserDetailServiceImpl implements UserDetailsService {

    @Resource
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String parameter) throws UsernameNotFoundException {
        User user = userMapper.getUserByUsernameOrMobile(parameter);
        if (Objects.isNull(user)) {
            throw new UsernameNotFoundException("当前登录和用户不存在");
        }
        // 根据用户id查询用户的权限
        Set<String> permissions = userMapper.selectPermissions(user.getId());
        String[] permissionArray = new String[permissions.size()];
        permissions.toArray(permissionArray);

        // 构建UserDetails
        UserDetails userDetails = org.springframework.security.core.userdetails.User.withUsername(user.getUsername())
                .password(user.getPassword())
                .disabled(!user.getEnable())
                .authorities(permissionArray)
                .build();
        return userDetails;
    }

}
