package com.krisdad.user.controller;

import com.krisdad.commom.bean.ResponseResult;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/27 11:15
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @PostMapping()
    @PreAuthorize("hasAuthority('新建用户')")
    public ResponseResult add() {
        return ResponseResult.success();
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('删除用户')")
    public ResponseResult delete(@PathVariable("id") String id) {
        return ResponseResult.success(id);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('修改用户')")
    public ResponseResult update(@PathVariable("id") String id) {
        return ResponseResult.success(id);
    }

    @GetMapping("/{id}")
    @PreAuthorize("permitAll()")
    public ResponseResult select(@PathVariable("id") String id) {
        return ResponseResult.success(id);
    }

}
