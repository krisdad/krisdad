package com.krisdad.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * Token 配置项
 *
 * @author krisdad
 * @version 1.0
 * @date 2021/5/27 14:57
 */
@Configuration
public class TokenConfig {

    private String SIGNING_KEY = "krisdad";

    /**
     * Token 存储方式
     * InMemoryTokenStore 存储在内存里面
     * RedisTokenStore 存储在Redis里面
     * JdbcTokenStore 存储在数据库里面
     * JwtTokenStore JWT token
     *
     * @return
     */
    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        //对称秘钥，资源服务器使用该秘钥来验证
        converter.setSigningKey(SIGNING_KEY);
        return converter;
    }
}
