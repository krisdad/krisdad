package com.krisdad.test.juc.synchronize;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author HLX
 * @version 1.0
 * @date 2021/5/7 17:40
 */
@Slf4j
public class LockCoarseningTest {

    @Test
    public void test1() {
        // 优化前
        synchronized (this) {
            // do something 1
        }

        // do something 2

        synchronized (this) {
            // do something 3
        }
    }

    @Test
    public void test2() {
        // 优化后
        synchronized (this) {
            // do something 1
            // do something 2
            // do something 3
        }
    }

    @Test
    public void test3() {
        // 优化前
        for (int i = 0; i < 10; i++) {
            synchronized (this) {
                // do something
            }
        }
    }

    @Test
    public void test4() {
        // 优化后
        synchronized (this) {
            for (int i = 0; i < 10; i++) {
                // do something
            }
        }
    }
}
