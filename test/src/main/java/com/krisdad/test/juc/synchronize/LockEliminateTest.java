package com.krisdad.test.juc.synchronize;

import lombok.extern.slf4j.Slf4j;

/**
 * @author HLX
 * @version 1.0
 * @date 2021/5/7 17:58
 */
@Slf4j
public class LockEliminateTest {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        int size = 1000000;
        for (int i = 0; i < size; i++) {
            createStringBuffer("test", "tes1231231231231t");
        }
        long timeCost = System.currentTimeMillis() - start;
        System.out.println("createStringBuffer:" + timeCost + " ms");
    }

    public static String createStringBuffer(String str1, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        // append方法是同步操作
        stringBuffer.append(str1);
        stringBuffer.append(str2);
        return stringBuffer.toString();
    }


}
