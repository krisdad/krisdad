package com.krisdad.test.juc.synchronize;

import org.junit.Test;
import org.openjdk.jol.info.ClassLayout;

/**
 * @author HLX
 * @version 1.0
 * @date 2021/5/7 9:36
 */
public class ObjectHeaderTest {

    @Test
    public void test1(){
        Object object = new Object();
        System.out.println(ClassLayout.parseInstance(object).toPrintable());

        int[] array = new int[]{1,2};
        System.out.println(ClassLayout.parseInstance(array).toPrintable());
    }

}
