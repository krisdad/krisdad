package com.krisdad.security.dao;

import com.krisdad.security.bean.Permission;
import com.krisdad.security.bean.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.Set;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/19 15:19
 */
@Mapper
public interface UserMapper {

    /**
     * 通过用户名或手机号获取用户实体
     * @param parameter 参数
     * @return 返回结果
     */
    User getUserByUsernameOrMobile(String parameter);

    /**
     * 创建用户
     * @param user 用户实体
     * @return 创建结果
     */
    int addUser(User user);

    /**
     * 获取用户的所有权限
     * @param userId 用户id
     * @return 用户的权限列表
     */
    Set<String> selectPermissions(Integer userId);
}
