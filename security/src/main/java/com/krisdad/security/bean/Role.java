package com.krisdad.security.bean;

import lombok.Data;

import java.util.Set;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/19 15:15
 */
@Data
public class Role {

    private Integer id;
    private String name;
    private Set<Permission> permissions;

}
