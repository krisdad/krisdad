package com.krisdad.security.bean;

import lombok.Data;

import java.util.Set;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/19 15:12
 */
@Data
public class User {

    private Integer id;
    private String username;
    private String password;
    private String mobile;
    private Boolean enable;
    private Set<Integer> roles;

}
