package com.krisdad.security.bean;

import lombok.Data;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/19 15:13
 */
@Data
public class Permission {

    private Integer id;
    private String description;
    private String url;

}
