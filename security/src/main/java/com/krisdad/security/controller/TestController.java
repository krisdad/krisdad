package com.krisdad.security.controller;

import com.krisdad.security.utils.ResponseResult;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/18 16:49
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/v1")
    @PreAuthorize("hasAuthority('权限1')")
    public ResponseResult function1(){
        return ResponseResult.success();
    }

    @GetMapping("/v2")
    @PreAuthorize("hasAuthority('权限2')")
    public ResponseResult function2(){
        return ResponseResult.success();
    }

    @GetMapping("/v3")
    @PreAuthorize("hasAuthority('权限3')")
    public ResponseResult function3(){
        return ResponseResult.success();
    }

}
