package com.krisdad.security.utils;

import java.util.HashMap;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/8 11:30
 */
public class ResponseResult extends HashMap<String, Object> {

  public static String SUCCESS_CODE = "200";
  public static String ERROR_CODE = "500";
  public static String DATA_KEY = "data";
  public static String MSG_KEY = "msg";

  private ResponseResult() {}

  public ResponseResult set(String key, Object object) {
    super.put(key, object);
    return this;
  }

  private static ResponseResult newResponseResult() {
    return new ResponseResult();
  }

  public static ResponseResult success() {
    return ResponseResult.newResponseResult()
        .set("code", ResponseResult.SUCCESS_CODE)
        .set(ResponseResult.MSG_KEY, "success");
  }

  public static ResponseResult success(Object object) {
    return ResponseResult.newResponseResult()
        .set("code", ResponseResult.SUCCESS_CODE)
        .set(ResponseResult.MSG_KEY, "success")
        .set(ResponseResult.DATA_KEY, object);
  }

  public static ResponseResult success(String msg) {
    return ResponseResult.newResponseResult()
        .set("code", ResponseResult.SUCCESS_CODE)
        .set(ResponseResult.MSG_KEY, msg);
  }

  public static ResponseResult success(String msg, Object object) {

    return ResponseResult.newResponseResult()
        .set("code", ResponseResult.SUCCESS_CODE)
        .set(ResponseResult.MSG_KEY, msg)
        .set(ResponseResult.DATA_KEY, object);
  }

  public ResponseResult data(Object obj) {
    return this.set("data", obj);
  }

  public static ResponseResult error() {
    return ResponseResult.newResponseResult()
        .set(ResponseResult.MSG_KEY, "failed")
        .set("code", ResponseResult.ERROR_CODE);
  }

  public static ResponseResult error(Object object) {
    return ResponseResult.newResponseResult()
        .set(ResponseResult.MSG_KEY, "failed")
        .set(ResponseResult.DATA_KEY, object)
        .set("code", ResponseResult.ERROR_CODE);
  }

  public static ResponseResult error(String msg) {
    return ResponseResult.newResponseResult()
        .set(ResponseResult.MSG_KEY, msg)
        .set("code", ResponseResult.ERROR_CODE);
  }

  public static ResponseResult error(String msg, Object object) {
    return ResponseResult.newResponseResult()
        .set(ResponseResult.MSG_KEY, msg)
        .set(ResponseResult.DATA_KEY, object)
        .set("code", ResponseResult.ERROR_CODE);
  }
}
