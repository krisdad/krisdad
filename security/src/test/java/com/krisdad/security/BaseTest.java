package com.krisdad.security;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/20 9:18
 */
@SpringBootTest()
@RunWith(SpringRunner.class)
public class BaseTest {
}
