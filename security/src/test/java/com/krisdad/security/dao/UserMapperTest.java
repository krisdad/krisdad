package com.krisdad.security.dao;

import com.krisdad.security.BaseTest;
import com.krisdad.security.bean.User;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author krisdad
 * @version 1.0
 * @date 2021/5/20 9:17
 */
class UserMapperTest extends BaseTest {

    @Resource
    private UserMapper userMapper;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Test
    void getUserByUsernameOrMobile() {
        User user1 = userMapper.getUserByUsernameOrMobile("18996470535");
        User user2 = userMapper.getUserByUsernameOrMobile("krisdad");
        System.out.println("user1 = " + user1);
        System.out.println("user2 = " + user2);
    }

    @Test
    void addUser() {
        User user = new User();
        user.setUsername("krisdad");
        user.setPassword("123");
        user.setMobile("18996470535");
        user.setEnable(true);
        String password = passwordEncoder.encode(user.getPassword());
        user.setPassword(password);
        assertEquals(1, userMapper.addUser(user));
    }

    @Test
    void selectPermissions() {
        Set<String> permissions = userMapper.selectPermissions(1);
        System.out.println("permissions = " + permissions);
    }
}
